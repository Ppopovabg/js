const socket = io.connect('/'),
    player = document.getElementById('player'),
    chat = document.getElementById('chat'),
    msgbox = document.getElementById('msg');

const colorsArray = ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
'#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
'#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
'#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
'#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
'#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
'#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
'#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
'#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
'#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'];

var tick = 0;
var userColor = generateColor();
var checkSync = true;
var cid = generateId(5);

window.onload = () => {
    updateSourceField();

    socket.emit('message', {from: cid, msg: 'connected'});
    socket.emit('request_seek');
}

function updateSourceField() {
    const videoUrl = player.querySelector('source').attributes.src.nodeValue;
    var nowPlaying = 'Nothing playing';
    if (videoUrl.length > 0 && videoUrl.indexOf(".") != -1) {
        nowPlaying = 'playing: ' + videoUrl;
    }
    document.getElementById('fileinfo').innerText = nowPlaying;
}

const MSG_TYPE_CHAT = 0, MSG_TYPE_CMD = 1, MSG_TYPE_LOAD = 2;

socket.on('new_message', data => {
    var html = "";
    switch (data.type) {
    case MSG_TYPE_CHAT:
        html = `<p class="chat"><span style="color:${data.color}"><strong>${data.from}</strong></span>: ${data.msg}'</p>`;
        break;
    case MSG_TYPE_LOAD:
        html = `<p style="color:aqua">${data.from}: ${data.msg}</p>`;
        break;
    default:
        html = `<p>${data.from}: ${data.msg}</p>`;
        break;
    }
    chat.innerHTML += html;
    chat.scrollTop = chat.scrollHeight;
});

socket.on('request_sync', data => {
    var delta = Math.abs(player.currentTime - data.time);
    if (delta > 30) {
        player.currentTime = data.time;
        socket.emit('message', {from: cid, msg: 'synced'});
    }
});

socket.on('pause', () => player.pause());
socket.on('play', () => player.play());
socket.on('update_clients', data => document.getElementById('clients').innerText = data.connected + ' clients connected');
socket.on('disconn', () => socket.emit('message', {from: cid, msg: 'disconnected'}));

socket.on('requested_seek', () => socket.emit('seek_others', {
    time: player.currentTime,
    state: player.paused,
    file: player.querySelector('source').attributes.src.nodeValue,
    subtitles: player.querySelector('track') ? player.querySelector('track').src : null
}));

socket.on('seek', data => {
    player.currentTime = data.time;
    if (data.state) {
        player.pause();
    } else {
        player.play();
    }

    if (player.querySelector('source').attributes.src.nodeValue != data.file) {
        changeVideo(data.file);
    }

    if (player.querySelector('track') == null || player.querySelector('track').src != data.subtitles) {
        console.log(data.subtitles);
        loadSubs(data.subtitles);
    }
});

player.addEventListener("pause", () => {
    if (player.seeking) return;

    socket.emit('broadcast', 'pause');
    socket.emit('message', {from: cid, msg: 'paused the video'});
});

player.addEventListener("play", () => {
    if (player.seeking) return;

    socket.emit('broadcast', 'play');
    socket.emit('message', {from: cid, msg: 'played the video'});
});

/*player.addEventListener('play', function firstPlay() {
    // seek on first play in case someone else is already playing the video
    socket.emit('request_seek');

    player.removeEventListener('play', firstPlay, false);
});*/

player.addEventListener("timeupdate", () => {
    var secs = Math.floor(player.currentTime);
    // every 30 secs check if in sync
    if (tick != secs && (secs % 300) == 0 && checkSync) {
        socket.emit('check_sync', {time: player.currentTime});
        tick = secs;
    }
});

player.addEventListener('loadedmetadata', () => {
    if (player.textTracks.length > 0) {
        var cues = player.textTracks[0].cues;
        for (let i = 0; i < cues.length; i++) {
            var cue = cues[i];
            if (new RegExp(/\n/).test(cue.text)) {
                cue.line = -3;
            } else {
                cue.line = -2;
            }
        }
    }
});

socket.on('changeVideo', data => {
    console.log(data.file);
    changeVideo(data.file);
});

socket.on('loadSubs', data => {
    console.log(data.file);
    loadSubs(data.file);
});

msgbox.addEventListener('keyup', evt => {
    if (evt.keyCode == 13 &&  msgbox.value) {
        var text = msgbox.value;
        if (text.startsWith("/")) {
            var split = text.split(" ");
            var cmdName = split[0].substring(1);
            var args = split.shift();

            switch (cmdName) {
            case "name":
                if (args.length > 1) {
                    cid = args.join(' ');
                }
                break;
            case "play":
                if (args.length > 1) {
                    var path = args.join(' ');
                    if (path.indexOf(":") == -1) {
                        return;
                    }

                    var folder = path.split(":")[0];
                    var file = path.split(":")[1];

                    socket.emit('change_subs', {file: `sub/${folder}/${file}.vtt`});
                    socket.emit('message', {from: "SERVER", msg: `Loaded subs ${file}.vtt`, type: MSG_TYPE_LOAD});
                    socket.emit('change_video', {file: `vid/${folder}/${file}.mp4`});
                    socket.emit('message', {from: "SERVER", msg: `Loaded video ${file}.mp4`, type: MSG_TYPE_LOAD});
                }
                break;
            case "syncon":
                checkSync = true;
                break;
            case "syncoff":
                checkSync = false;
                break;
            default:
                socket.emit('message', {from: "SERVER", msg: "Unknown command: " + args[0], type: MSG_TYPE_CMD});
                break;
            }
        } else {
            socket.emit('message', {from: cid, color: userColor, msg: text, type: MSG_TYPE_CHAT});
        }
        msgbox.value = '';
    }
});

function changeVideo(src) {
    player.pause();

    var source = player.querySelector('source');
    source.setAttribute('src', src); 

    player.load();
    //player.play();

    updateSourceField();
}

function loadSubs(src) {
    player.pause();

    var track = player.querySelector('track');
    if (track) {
        track.src = src;
    } else {
        player.innerHTML += '<track kind="subtitles" srclang="en" label="English" src="'+src+'" default />';
    }
}

function generateId(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function generateColor() {
    return colorsArray[Math.floor(Math.random() * colorsArray.length)];
}