var bgcanvas = document.getElementById("main-canvas"),
	bgctx = bgcanvas.getContext("2d"),
	txtcanvas = document.getElementById("text-canvas"),
	txtctx = txtcanvas.getContext("2d"),
	rectcanvas = document.getElementById("roundrect-canvas"),
	credctx = rectcanvas.getContext("2d"),
	mainTextarea = document.getElementById("input-text"),
	submitButton = document.getElementById('submit-button'),
	userBackground = document.createElement("img"),
	userMask = document.createElement("img");

var maskArray, bgArray, mask;

var texts = [];
var selectedText = 0;

function drawRoundRectText() {
	if(!useroundrectBox.checked) return;
	var colors1 = ["#f485a1", "#0a6e9b", "#f49677", "#d0424b", "#d673c8", "#879d50", "#7193b5", "#918ac6", "#f2549b", "#20536f", "#6ba3d3", "#e8b0ca", "#f49174", "#f26d77"];
	var colors2 = ["#6483a4", "#8f8bc6", "#ed6d8b", "#f4955d", "#e686b9", "#598392", "#f591c1", "#0a6e99", "#d22eac", "#98a1ba", "#ba9fd6", "#d6a2da", "#ed6d8a", "#7f6977"];
	var overlaysCount = colors1.length;
	for (var i = 0; i < overlaysCount; i++) {
		if(mask.src.includes("overlays/" + (i + 1) + ".png")) {
			color1 = colors1[i];
			color2 = colors2[i];
		}
	}
	var padding = 25;
	var x = parseInt(roundrectX.value) + padding;
	var y = parseInt(roundrectY.value) + padding;
	formatRoundRectText(x, y, color1, color2);
}

function drawRoundRectBox() {
	credctx.clearRect(0, 0, txtcanvas.width, txtcanvas.height);
	credctx.save();
	credctx.fillStyle = "#ffffff";
	roundRect(credctx, parseInt(roundrectX.value), parseInt(roundrectY.value), parseInt(roundrectWidth.value), 80, 50, true, false);
	if(useDoublesBox.checked) {
		credctx.fillStyle = "#ffffff";
		roundRect(credctx, parseInt(roundrectX.value) + 50, parseInt(roundrectY.value) + 65, parseInt(roundrectWidth.value), 80, 50, true, false);
	}
	drawRoundRectText();
}

function selectUserImage(image, type) {
	var userImage = image.childNodes[1];
	var path = userImage.src;
	var filename = path.substr(path.length - 4);
	var index = filename.charAt(0);
	var array = [];
	if(type === "background") {
		array = bgArray;
		userBackground.src = userImage.src;
	} else if(type === "overlay") {
		array = maskArray;
		userMask.src = userImage.src;
	}
	path = "assets/" + type + "s/";
	var images = [];
	for (var i = 0; i < array.length; i++) {
		images[i] = document.querySelectorAll('#' + type + ' div img')[i];
	}
	for (var i = 0; i < array.length; i++) {
		for (img of images) {
			if(img.style.boxShadow !== "none" && img.src !== userImage.src) {
				img.style.boxShadow = "none";
			} else if(img.src === userImage.src) {
				userImage.style.boxShadow = "0 0 15px 2px rgba(0, 0, 0, .5)";
			}
		}
	}
}

// https://gist.github.com/chriskoch/366054
function drawCanvasText(x, y, doStroke, text) {
	text = mainTextarea.value;
	var lines = text.split("\n");
	var padding = parseInt(lineSpacing.value);
	//ctx.save();
	//ctx.translate(posX, posY);
	txtctx.fillStyle = "black";
	txtctx.textBaseline = "top";
	if(texts.length === 0) {
		for (i = 0; i < lines.length; i++) {
			switch(i) {
				case 0:
					txtctx.font = fontSize1.value + "pt crewniverse_font";
					var lineHeight1 = parseInt(txtctx.font);
					break;
				case 1:
					txtctx.font = fontSize2.value + "pt crewniverse_font";
					var lineHeight2 = parseInt(txtctx.font);
					y = parseInt(y) + lineHeight1 + padding;
					break;
				default:
					txtctx.font = fontSize3.value + "pt crewniverse_font";
					y = parseInt(y) + lineHeight2 + padding;
					break;
			}
			(doStroke) ? txtctx.strokeText(lines[i], x, y) : txtctx.fillText(lines[i], x, y);
			console.log(text);
			texts.push({
				text: lines[i],
				x: x,
				y: y,
				width: txtctx.measureText(lines[i]).width,
				height: parseInt(txtctx.font)
			});
		}
		return;
	}
	// update lines
	for (i = 0; i < texts.length; i++) {
		texts[i].text = lines[i];
		if(lines.length > texts.length) {
			let i2 = (i==0) ? i : i-1;
			texts.push({
				text: lines[i],
				x: texts[i2].x,
				y: texts[i2].y+texts[i2].height,
				width: txtctx.measureText(lines[i]).width,
				height: parseInt(txtctx.font)
			});
		}
	}

	for (var i = texts.length - 1; i >= 0; i--) {
		switch(i) {
			case 0:
				txtctx.font = fontSize1.value + "pt crewniverse_font";
				var lineHeight1 = parseInt(txtctx.font);
				break;
			case 1:
				txtctx.font = fontSize2.value + "pt crewniverse_font";
				var lineHeight2 = parseInt(txtctx.font);
				y = parseInt(y) + lineHeight1 + padding;
				break;
			default:
				txtctx.font = fontSize3.value + "pt crewniverse_font";
				y = parseInt(y) + lineHeight2 + padding;
				break;
		}
		(doStroke) ? txtctx.strokeText(texts[i].text, texts[i].x, texts[i].y) : txtctx.fillText(texts[i].text, texts[i].x, texts[i].y);
		texts[i].width = txtctx.measureText(texts[i].text).width;
		texts[i].height = parseInt(txtctx.font);
	}
}

function updateCanvasText(x, y) {
	//var x = inputX.value;
	//var y = inputY.value;
	var text = (texts.length > 0 && texts[selectedText]) ? texts[selectedText].text : mainTextarea.value;
	var mask_X = (calcMaskPos.checked) ? inputX.value : maskY.value;
	var mask_Y = (calcMaskPos.checked) ? 450 - (inputY.value * 1.5) : maskY.value;
	txtctx.clearRect(0, 0, txtcanvas.width, txtcanvas.height);
	txtctx.save();
	if(texts[selectedText]) console.log(texts[selectedText].text);
	drawCanvasText(x, y, false, text);
	txtctx.globalCompositeOperation = "source-in";
	txtctx.drawImage(mask, mask_X, mask_Y, 1200, mask.height, 0, 0, bgcanvas.width, bgcanvas.height);
	txtctx.globalCompositeOperation = "destination-over";
	txtctx.strokeStyle = 'white';
	txtctx.lineWidth = strokeWidth.value;
	txtctx.lineJoin = 'round';
	drawCanvasText(x, y, true, text);
	txtctx.shadowColor = "#d5efed";
	txtctx.shadowOffsetX = 0;
	txtctx.shadowOffsetY = shadowOffset.value;
	txtctx.shadowBlur = 0;
	drawCanvasText(x, y, true, text);
	//console.log(x);
	//console.log(y);
	txtctx.restore();
}

function getRandomImage(array, path) {
	return path + array[Math.floor(Math.random() * array.length)];
}

function updateCanvas(caller, x = 0, y = 0) {
	bgctx.clearRect(0, 0, bgcanvas.width, bgcanvas.height);
	txtctx.clearRect(0, 0, bgcanvas.width, bgcanvas.height);
	credctx.clearRect(0, 0, bgcanvas.width, bgcanvas.height);
	var backgroundImage = new Image();
	mask = new Image();
	mask.src = (userMask.src == "") ? getRandomImage(maskArray, "assets/overlays/") : userMask.src;
	backgroundImage.src = (userBackground.src == "") ? getRandomImage(bgArray, 'assets/titlecards/') : userBackground.src;
	
	if(x === 0 && y === 0) {
		x = inputX.value;
		y = inputY.value;
	}

	if(useRandBg.checked && caller === "button") {
		backgroundImage.src = getRandomImage(bgArray, 'assets/titlecards/');
	}
	if(useRandMask.checked && caller === "button") {
		mask.src = getRandomImage(maskArray, "assets/overlays/");
	}
	if(backgroundImage.complete) {
		bgctx.drawImage(backgroundImage, 0, 0, bgcanvas.width, bgcanvas.height);
		if(mask.complete) {
			updateCanvasText(x, y);
		} else {
			mask.onload = function() {
				updateCanvasText(x, y);
			};
		}
		if(useroundrectBox.checked) drawRoundRectBox();
		bgctx.drawImage(rectcanvas, 0, 0);
		bgctx.drawImage(txtcanvas, 0, 0);
	} else {
		backgroundImage.onload = function() {
			bgctx.drawImage(backgroundImage, 0, 0, bgcanvas.width, bgcanvas.height);
			if(mask.complete) {
				updateCanvasText(x, y);
			} else {
				mask.onload = function() {
					updateCanvasText(x, y);
				};
			}
			if(useroundrectBox.checked) drawRoundRectBox();
			bgctx.drawImage(rectcanvas, 0, 0);
			bgctx.drawImage(txtcanvas, 0, 0);
		};
	}
	if(caller === "button") updateCanvasHue();
	//drawRoundRectBox();
	//drawRoundRectText();
}

function updateCanvasHue() {
	if(useRandHue.checked) {
		var deg = Math.floor(Math.random() * 360);
		bgctx.filter = "hue-rotate(" + deg + "deg)";
	} else {
		var deg = hueDeg.value;
		bgctx.filter = "hue-rotate(" + deg + "deg)";
	}
}

function initArrays() {
	maskArray = new Array(14);
	for (var i = 1; i <= maskArray.length; i++) {
		maskArray[i-1] = i + '.png';
	}
	bgArray = new Array(22);
	for (var i = 1; i <= bgArray.length; i++) {
		bgArray[i-1] = i + '.png';
	}
}


function init() {
	initArrays();
	window.onload = function() {
		var canvas = bgcanvas;
		var offsetX = canvas.offsetLeft;
		var offsetY = canvas.offsetTop;
		var scrollX = canvas.scrollLeft;
		var scrollY = canvas.scrollTop;

		var startX;
		var startY;

		let tempMousePos;

		function textHittest(x, y, textIndex) {
		    var text = texts[textIndex];
		    return(
				x>=text.x &&
		        x<=(parseInt(text.x)+parseInt(text.width)) &&
		        y<=(parseInt(text.y)+parseInt(text.height)) &&
		        y>=text.y
			);
		}

		function getMousePos(canvas, evt) {
		    var rect = canvas.getBoundingClientRect();
		    return {
		      x: evt.clientX - rect.left,
		      y: evt.clientY - rect.top
		    };
		}

		function mouseDown(e) {
			e.preventDefault();
			startX = getMousePos(canvas, e).x;
			startY = getMousePos(canvas, e).y;
			for(var i=0; i < texts.length; i++) {
				if(textHittest(startX, startY, i)) {
					console.log(i);
					selectedText=i;
					tempMousePos = {x: getMousePos(canvas, e).x, y: getMousePos(canvas, e).y};
					return;
				}
			}
			selectedText=-1;
		}

		function mouseUp(e) {
			e.preventDefault();
			selectedText=-1;
		}

		// also done dragging
		function mouseOut(e) {
			e.preventDefault();
			selectedText=-1;
		}

		function mouseMove(e) {
			if(selectedText<0){return;}
			e.preventDefault();
			let amp = 5;
			let text = texts[selectedText];
			let minFontsize = 10;
			let fontsize = document.getElementById("font-size-" + (selectedText+1));
			if(e.ctrlKey) {
				let currMousePos = {x: getMousePos(canvas, e).x, y: getMousePos(canvas, e).y};
				if(currMousePos.x > tempMousePos.x) {
					fontsize.value = parseInt(fontsize.value) + amp;
					updateCanvas("mouse", parseInt(text.x), parseInt(text.y));
				} else if(currMousePos.x < tempMousePos.x) {
					if(minFontsize < (fontsize.value - amp)) fontsize.value -= amp;
					updateCanvas("mouse", parseInt(text.x), parseInt(text.y));
				}
			} else {
				mouseX = getMousePos(canvas, e).x;
				mouseY = getMousePos(canvas, e).y;
				var dx = parseInt(mouseX) - parseInt(startX);
				var dy = parseInt(mouseY) - parseInt(startY);
				startX = parseInt(mouseX);
				startY = parseInt(mouseY);
				text.x = parseInt(text.x);
				text.y = parseInt(text.y);
				text.x += parseInt(dx);
				text.y += parseInt(dy);
				console.log("mousemove");
				console.log(text);
				updateCanvas("mouse", parseInt(text.x), parseInt(text.y));
			}
		}

		canvas.onmousedown = function(e) {
			mouseDown(e);
		}
		canvas.onmouseup = function(e) {
			mouseUp(e);
		}
		canvas.onmousemove = function(e) {
			mouseMove(e);
		}
		canvas.onmouseout = function(e) {
			mouseOut(e);
		}
		var tempcount = 0;
		var maxlines = 3;
		mainTextarea.onkeyup = function(evt) {
			linebreaks = (mainTextarea.value.match(/\n/g)||[]).length;
			if(linebreaks > 0 && linebreaks < maxlines) {
				if(linebreaks === maxlines - 1) {
					document.getElementById("fontsize2").style.display = "block";
					document.getElementById("fontsize3").style.display = "block";
				} else if(linebreaks > tempcount) {
					document.getElementById("fontsize" + (linebreaks + 1)).style.display = "block";
				} else if(linebreaks < tempcount) {
					document.getElementById("fontsize" + (tempcount + 1)).style.display = "none";
				}
			} else if(linebreaks === 0 && linebreaks < tempcount) {
				document.getElementById("fontsize2").style.display = "none";
				document.getElementById("fontsize3").style.display = "none";
			}
			tempcount = linebreaks;
		};
	};
	submitButton.addEventListener('click', function() {
		updateCanvas("button");
	});
}

init();