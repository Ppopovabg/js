const http = require('http'),
      static = require('node-static'),
      file = new static.Server('./'),
      server = http.createServer((req, res) => {
        req.addListener('end', () => file.serve(req, res)).resume();
      }),
      io = require('socket.io')(server),
      port = 5210;

var connectedCount = 0;

io.on('connect', socket => {
  connectedCount++;
  socket.on('message',      data          => io.sockets.emit('new_message', data)); 
  socket.on('broadcast',    (event, data) => socket.broadcast.emit(event, data));
  socket.on('check_sync',   data          => io.sockets.emit('request_sync', data));
  socket.on('change_video', data          => io.sockets.emit('changeVideo', data));
  socket.on('change_subs',  data          => io.sockets.emit('loadSubs', data));
  socket.on('seek_others',  data          => socket.broadcast.emit('seek', data));
  socket.on('disconnect', () => {
    connectedCount--;
    socket.broadcast.emit('update_clients', {connected: connectedCount});
    socket.emit('disconn');
  });

  io.sockets.emit('update_clients', {connected: connectedCount});

  if (connectedCount > 1) {
    var controllerSock = getControllerSocket();

    socket.on('request_seek', () => controllerSock.emit('requested_seek'));
  }
});

server.listen(port, () => console.log(`Server running at http://localhost:${port}`));

function getControllerSocket() {
  return Object.values(io.of("/").connected)[0];
}